﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    // TODO - убрать версионность из routes    
    //      - мигрировать на .Net 6

    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> EmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();                      

            return employeesModelList;
        }        

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> EmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создание сотрудника
        /// </summary>
        /// <param name="emploeeCreateRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateEmployee(EmploeeCreateUpdateRequest emploeeCreateRequest)
        {
            // Метод создания и изменения не позволяют задавать поля AppliedPromocodesCount и Roles,
            // так как в моём виденье Роли работника задаются отдельно от процесса его создания,
            // а AppliedPromocodesCount - это что-то такое, что должно вычисляться на лету на основе пока несуществующих в приложении Promocodes.

            var newEmployee = new Employee()
            {
                FirstName = emploeeCreateRequest.FirstName,
                LastName = emploeeCreateRequest.LastName,
                Email = emploeeCreateRequest.Email,
                AppliedPromocodesCount = 0,
                Roles = new List<Role>()
            };

            await _employeeRepository.CreateAsync(newEmployee);

            return Ok();
        }

        /// <summary>
        /// Обновление сотрудника
        /// </summary>
        /// <param name="emploeeCreateUpdateRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateEmployee(EmploeeCreateUpdateRequest emploeeCreateUpdateRequest)
        {
            // Метод не позвоялет изменить email , так как в моём виденье это отдельный процесс

            Employee foundEmployee =
                await _employeeRepository.GetByIdAsync((Guid)emploeeCreateUpdateRequest.Id);

            if (foundEmployee == null)
            {
                return BadRequest("Employee with requested id not found");
            }
            else
            {
                var employeeToUpdate = new Employee()
                {
                    Id = foundEmployee.Id,
                    FirstName = emploeeCreateUpdateRequest.FirstName,
                    LastName = emploeeCreateUpdateRequest.LastName,
                    Email = foundEmployee.Email,
                    AppliedPromocodesCount = foundEmployee.AppliedPromocodesCount,
                    Roles = foundEmployee.Roles
                };

                await _employeeRepository.UpdateAsync(employeeToUpdate);

                return Ok();
            }
        }

        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteEmployee(Guid id)
        {
            await _employeeRepository.DeleteByIdAsync(id);

            return Ok();
        }
    }
}