﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected ICollection<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task DeleteByIdAsync(Guid id)
        {
            if (Data.Where(a => a.Id == id).FirstOrDefault() is BaseEntity entity)
            {
                Data.Remove((T)entity);
            }
            
            return Task.CompletedTask;
        }

        public Task CreateAsync(T entity)
        {   
            entity.Id = Guid.NewGuid();
            Data.Add(entity);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {
            var foundEntity = Data.Where(a => a.Id == entity.Id).FirstOrDefault();
            if (foundEntity != null)
            {
                Data.Remove(foundEntity);
                Data.Add(entity);                
                return Task.CompletedTask;
            }
            else
            {
                // TODO - replace with new class of Exception
                throw new Exception("Entity not found in Repository");
            }
        }
    }
}